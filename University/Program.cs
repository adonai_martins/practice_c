﻿using System; // this is like import
using System.ComponentModel;

namespace University //namespace is like package
{
    class Program
    {
        static void Main(string[] args)
        {

            for (int index = 1; index<100; index++) {

                if (index % 2 == 0) continue;

                Console.WriteLine(index);
            
            }
        }
    }
}
